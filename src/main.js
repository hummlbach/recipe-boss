import 'font-awesome/css/font-awesome.min.css';
import '@/styles/main.scss';

import Vue from 'vue';
import VueHead from 'vue-head';
import VModal from 'vue-js-modal';
import Gettext from 'vue-gettext';
import VueLazyload from 'vue-lazyload';
import { FlowerSpinner } from 'epic-spinners';

// TODO see if this can be baked into the build process
import flatMap from 'array.prototype.flatmap';

import App from './App';
import router from './router';
import EventBus from './bus';
import StarRating from './components/StarRating';
import Loading from './components/Loading';

import translations from './translations.json';

flatMap.shim();

Vue.config.productionTip = false;

Object.defineProperties(Vue.prototype, {
    $bus: {
        get: function() {
            return EventBus;
        },
    },
});

Vue.use(VueHead, {
    separator: '-',
    complement: 'Recipe Boss',
});
Vue.use(VModal);
Vue.use(Gettext, {
    availableLanguages: {
        en_US: 'English',
        ca: 'Catalan',
        de: 'German',
        es: 'Spanish',
        ru: 'Russian',
        nl: 'Dutch',
    },
    translations: translations,
    silent: true,
});
Vue.use(VueLazyload, {
    error: '/img/missing.svg',
    loading: '/img/missing.svg',
});

Vue.component('star-rating', StarRating);
Vue.component('loading', Loading);
Vue.component('flower-spinner', FlowerSpinner);

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    render: (h) => h(App),
});
