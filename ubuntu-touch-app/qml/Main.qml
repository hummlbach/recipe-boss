import QtQuick 2.9
import QtWebEngine 1.7
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3
import Morph.Web 0.1

import "Components"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'recipe-boss.bhdouglass'
    automaticOrientation: true
    anchorToKeyboard: false // The osk is handled by the KeyboardRectangle since the UI toolkit doesn't respect scaling

    width: units.gu(45)
    height: units.gu(75)

    Settings {
        id: settings
        property string url: Qt.resolvedUrl('../www/index.html')
    }

    Page {
        id: page
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: keyboardRect.top
        }

        header: PageHeader {
            id: header

            // TODO enable this always and disable the header on the page
            visible: webview.isExternalLink
            title: 'Recipe Boss'

            navigationActions: Action {
                iconName: 'go-previous'
                text: i18n.tr('Back')
                enabled: webview.canGoBack
                onTriggered: webview.goBack()
            }
        }

        WebContext {
            id: webcontext
            userAgent: 'Mozilla/5.0 (Linux; Android 5.0; Nexus 5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.111 Mobile Safari/537.36 Ubuntu Touch Webapp'
        }

        WebView {
            id: webview
            anchors {
                top: isExternalLink ? header.top : parent.top
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            context: webcontext
            property bool isExternalLink: false

            onUrlChanged: {
                console.log('url changed: ' + url);

                if (url.toString().indexOf('https://recipes.bhdouglass.com/') === 0) {
                    webview.url = url.toString().replace('https://recipes.bhdouglass.com/', Qt.resolvedUrl('../www/index.html'));
                }

                isExternalLink = (url.toString().indexOf('file://') == -1);

                if (!isExternalLink) {
                    settings.url = url.toString();
                }
            }

            function navigationRequestedDelegate(request) {
                var url = request.url.toString();

                console.log('navigation requsted: ' + url);
                if (url.indexOf('https://recipes.bhdouglass.com/') === 0) {
                    request.action = WebEngineNavigationRequest.IgnoreRequest;
                    webview.url = url.replace('https://recipes.bhdouglass.com/', Qt.resolvedUrl('../www/index.html'));
                }
                else if (url.indexOf('##EXTERNAL##') >= 0) {
                    request.action = WebEngineNavigationRequest.IgnoreRequest;
                    Qt.openUrlExternally(url.replace('##EXTERNAL##', ''));
                }

                // We don't want to block external urls or that breaks access to remotestorage sites
            }

            Component.onCompleted: {
                var url = settings.url;
                if (url) {
                    webview.url = url.replace(/\/recipe-boss.bhdouglass\/(\d\.\d\.\d)/g, '/recipe-boss.bhdouglass/current');
                }
            }
        }
    }

    KeyboardRectangle {
        id: keyboardRect
    }

    function handleUri(uri) {
        console.log('handling uri: ' + uri);

        webview.url = uri.replace('https://recipes.bhdouglass.com/', Qt.resolvedUrl('../www/index.html'));
    }

    Connections {
        target: UriHandler
        onOpened: {
            handleUri(uris[0]);
        }
    }

    Component.onCompleted: {
        if (Qt.application.arguments[1] && Qt.application.arguments[1].indexOf('https://recipes.bhdouglass.com') === 0) {
            handleUri(Qt.application.arguments[1]);
        }
    }
}
