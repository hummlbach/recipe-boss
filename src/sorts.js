export function grocerySort(a, b) {
    if (a.name.toLowerCase() > b.name.toLowerCase()) {
        return 1;
    }

    if (a.name.toLowerCase() < b.name.toLowerCase()) {
        return -1;
    }

    return 0;
}

export function categorySort(a, b) {
    // Sort "no category" first
    if (!a.id) {
        return -1;
    }
    if (!b.id) {
        return 1;
    }

    // Sort no orders to the bottom and order them by name
    if (!a.order && a.order !== 0 && !b.order && b.order !== 0) {
        if (a.name.toLowerCase() > b.name.toLowerCase()) {
            return 1;
        }

        if (a.name.toLowerCase() < b.name.toLowerCase()) {
            return -1;
        }
    }
    if (!a.order && a.order !== 0) {
        return 1;
    }
    if (!b.order && b.order !== 0) {
        return -1;
    }

    // Otherwise sort by the user order
    if (a.order > b.order) {
        return 1;
    }
    if (a.order < b.order) {
        return -1;
    }

    return 0;
}
