export default {
    name: 'grocery_categories',
    builder: function(privateClient) {
        privateClient.declareType('grocery_category', {
            type: 'object',
            properties: {
                name: {type: 'string'},
                order: {type: 'number'},
                suggestions: {type: 'array'},
            },
            required: [
                'name',
            ],
        });

        return {
            exports: {
                init: () => {
                    privateClient.cache('');
                },

                on: privateClient.on,

                add: (category) => privateClient.storeObject('grocery_category', category.id, category),

                find: privateClient.getObject.bind(privateClient),

                delete: privateClient.remove.bind(privateClient),

                list: () => privateClient.getAll(''),
            },
        };
    },
};
