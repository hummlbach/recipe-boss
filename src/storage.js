import uuid from 'uuid/v4';

import remoteStorage from './remotestorage';
import EventBus from './bus';

function toValues(data) {
    return Object.values(data).filter((d) => !!d.id);
}

function parseLocalStorage(key) {
    let data = [];
    let lsData = window.localStorage.getItem(key);
    if (lsData) {
        try {
            data = JSON.parse(lsData);
        }
        catch (e) {
            data = {};
        }
    }

    return data;
}

let metadata = {};
function loadMetadata() {
    return remoteStorage.recipes.list().then((recipes) => {
        toValues(recipes).forEach((recipe) => {
            metadata[recipe.id] = {
                id: recipe.id,
                image: recipe.image,
                title: recipe.title,
                description: recipe.description,
                rating: recipe.rating,
            };
        });

        window.localStorage.setItem('metadata', JSON.stringify(metadata));
        return metadata;
    });
}

metadata = parseLocalStorage('metadata');
let localGroceries = parseLocalStorage('groceries');
let localGroceryCategories = parseLocalStorage('groceryCategories');

remoteStorage.on('disconnected', () => {
    metadata = {};
    window.localStorage.removeItem('metadata');

    EventBus.$emit('reload-recipes');
});

remoteStorage.on('connected', async () => {
    // Migrate localstorage groceries
    if (localGroceries && localGroceries.length > 0) {
        await Promise.all(localGroceries.map((grocery) => {
            grocery.quantity = grocery.quantity ? grocery.quantity : 0;
            return remoteStorage.groceries.add(grocery);
        }));

        window.localStorage.removeItem('groceries');
    }

    // Migrate localstorage grocery categories
    if (localGroceryCategories && localGroceryCategories.length > 0) {
        await Promise.all(localGroceryCategories.map((category) => {
            return remoteStorage.grocery_categories.add(category);
        }));

        window.localStorage.removeItem('groceryCategories');
    }

    EventBus.$emit('reload-groceries');

    loadMetadata().then(() => {
        EventBus.$emit('reload-recipes');
    });
});

remoteStorage.recipes.on('change', () => {
    loadMetadata().then(() => {
        EventBus.$emit('reload-recipes');
    });
});

remoteStorage.groceries.on('change', () => {
    EventBus.$emit('reload-groceries');
});

remoteStorage.grocery_categories.on('change', () => {
    EventBus.$emit('reload-groceries');
});

remoteStorage.recipes.init();
remoteStorage.groceries.init();
remoteStorage.grocery_categories.init();

export default {
    remoteStorage: remoteStorage,

    recipes: {
        save(recipe) {
            if (!recipe.id) {
                let id = uuid();
                recipe.id = id;
            }

            if (recipe.total_time !== null && recipe.total_time !== undefined) {
                recipe.total_time = parseInt(recipe.total_time, 10);
                if (Number.isNaN(recipe.total_time)) {
                    recipe.total_time = 0;
                }
            }

            if (recipe.prep_time !== null && recipe.prep_time !== undefined) {
                recipe.prep_time = parseInt(recipe.prep_time, 10);
                if (Number.isNaN(recipe.prep_time)) {
                    recipe.prep_time = 0;
                }
            }

            return remoteStorage.recipes.add(recipe).then(() => {
                metadata[recipe.id] = {
                    id: recipe.id,
                    image: recipe.image,
                    title: recipe.title,
                    description: recipe.description,
                    rating: recipe.rating,
                };
                window.localStorage.setItem('metadata', JSON.stringify(metadata));

                return recipe;
            });
        },
        remove(id) {
            return remoteStorage.recipes.delete(id).then(() => {
                delete metadata[id];
                window.localStorage.setItem('metadata', JSON.stringify(metadata));
            });
        },
        find(id) {
            return remoteStorage.recipes.find(id);
        },
        search(term) {
            return new Promise((resolve) => {
                let results = [];
                let metadataValues = Object.values(metadata);
                for (let i = 0; i < metadataValues.length; i++) {
                    let result = metadataValues[i];

                    if (term) {
                        let title = result.title ? result.title.toLowerCase() : '';
                        let description = result.description ? result.description.toLowerCase() : '';

                        if (
                            title.indexOf(term.toLowerCase()) >= 0 ||
                            description.indexOf(term.toLowerCase()) >= 0
                        ) {
                            results.push(result);
                        }
                    }
                    else {
                        results.push(result);
                    }
                }

                results = results.sort((a, b) => {
                    if (a.title > b.title) {
                        return 1;
                    }
                    if (a.title < b.title) {
                        return -1;
                    }

                    return 0;
                });

                resolve(results);
            });
        },
    },

    groceries: {
        save(grocery) {
            if (!grocery.id) {
                grocery.id = uuid();
            }

            return remoteStorage.groceries.add(grocery);
        },
        remove(id) {
            return remoteStorage.groceries.delete(id);
        },
        find(id) {
            return remoteStorage.groceries.find(id);
        },
        async list() {
            return toValues(await remoteStorage.groceries.list());
        },
    },

    groceryCategories: {
        save(category) {
            if (!category.id) {
                category.id = uuid();
            }

            return remoteStorage.grocery_categories.add(category);
        },
        async remove(id) {
            const groceries = toValues(await remoteStorage.groceries.list());

            let promises = groceries.filter((grocery) => (grocery.categoryId == id))
                .map((grocery) => {
                    grocery.categoryId = '';
                    return remoteStorage.groceries.add(grocery);
                });

            promises.push(remoteStorage.grocery_categories.delete(id));
            await Promise.all(promises);
        },
        find(id) {
            return remoteStorage.grocery_categories.find(id);
        },
        async list() {
            return toValues(await remoteStorage.grocery_categories.list());
        },
        async findBySuggestion(groceryName) {
            const groceryCategories = toValues(await remoteStorage.grocery_categories.list());

            return groceryCategories.find((category) => {
                if (category.suggestions) {
                    return !!category.suggestions.find((suggestion) => {
                        return suggestion.toLowerCase() == groceryName.toLowerCase();
                    });
                }

                return false;
            });
        },
    },
};
